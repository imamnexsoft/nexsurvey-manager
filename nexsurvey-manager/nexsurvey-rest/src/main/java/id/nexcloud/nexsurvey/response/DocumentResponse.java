//package id.nexcloud.nexsurvey.response;
//
//import com.fasterxml.jackson.annotation.JsonInclude;
//
//import lombok.Builder;
//import lombok.Data;
//
//@Data
//@Builder
//@JsonInclude(JsonInclude.Include.NON_NULL)
//public class DocumentResponse {
//
//	private Long id;
//	private long size;
//	private String name;
//	private String path;
//	private DocType docType;
//
//}
