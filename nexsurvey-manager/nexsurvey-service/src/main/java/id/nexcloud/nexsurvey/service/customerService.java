package id.nexcloud.nexsurvey.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import id.nexcloud.nexsurvey.entity.jpa.customer;
import id.nexcloud.nexsurvey.entity.jpa.noteStore;
import id.nexcloud.nexsurvey.repository.jpa.customerRepo;
import id.nexcloud.nexsurvey.repository.jpa.mslRepo;
import id.nexcloud.nexsurvey.repository.jpa.noteStoreRepo;
import id.nexcloud.nexsurvey.repository.jpa.productStoreRepo;
import id.nexcloud.nexsurvey.repository.jpa.promotionRepo;
import lombok.RequiredArgsConstructor;

@Service("customerService")
@RequiredArgsConstructor
public class customerService {

	@Autowired
	customerRepo customerRepo;

	@Autowired
	mslRepo mslRepo;

	@Autowired
	noteStoreRepo noteStoreRepo;

	@Autowired
	promotionRepo promotionRepo;

	@Autowired
	productStoreRepo productStoreRepo;

	public String findByPrincipalIDAndDistributorIDAndCustomerCode(String principalID, String distributorID,
			String customerID) {
		List<customer> listCust = customerRepo.findByPrincipalIDAndDistributorIDAndCustomerCode(principalID,
				distributorID, customerID);
		System.out.println("cek list = "+listCust.size());
		System.out.println("json = ");
		Gson g = new Gson();
		System.out.println("json2 = ");
		String json = g.toJson(listCust);
		System.out.println("json = "+json);
		return json;
	}
	public String findByPrincipalIDAndDistributorID(String principalID, String distributorID) {
		List<customer> listCust = customerRepo.findByPrincipalIDAndDistributorID(principalID,distributorID);
		listCust.forEach(c -> {
			c.setListMsl(null);
			c.setListNote(null);
			c.setListProd(null);
			c.setListPromo(null);
		});
		Gson g = new Gson();
		String json = g.toJson(listCust);
		return json;
	}

	@Transactional
	public void saveCustomer(customer cust) {
		customer c = customerRepo.findByPrincipalIDAndDistributorIDAndCustomerCodeAndVisitDate(cust.getPrincipalID(),
				cust.getDistributorID(), cust.getCustomerCode(), cust.getVisitDate());
		BeanUtils.copyProperties(cust, c);
		if (c != null) {
			customerRepo.delete(c);
			promotionRepo.deleteAll(c.getListPromo());
			mslRepo.deleteAll(c.getListMsl());
			noteStoreRepo.deleteAll(c.getListNote());
			productStoreRepo.deleteAll(c.getListProd());
		}
			customerRepo.saveAndFlush(cust);
			System.out.println(cust.getId());
			cust.getListPromo().forEach(promo -> promo.setCustomer_ID(cust.getId()));
			cust.getListMsl().forEach(msl -> msl.setCustomer_ID(cust.getId()));
			cust.getListNote().forEach(note -> note.setCustomer_ID(cust.getId()));
			cust.getListProd().forEach(prod -> prod.setCustomer_ID(cust.getId()));
			promotionRepo.saveAll(cust.getListPromo());
			mslRepo.saveAll(cust.getListMsl());
			noteStoreRepo.saveAll(cust.getListNote());
			productStoreRepo.saveAll(cust.getListProd());
	}
}
