package id.nexcloud.nexsurvey.entity.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.nexcloud.nexsurvey.entity.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "mustSellList")
public class mustSellList extends BaseEntity{
	
	@Column(name = "customer_ID")
	private Long customer_ID;
	
	@Column(name = "productCode")
	private String productCode;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "isAvailable")
	private String isAvailable;

	@Column(name = "productGroup1")
	private String productGroup1;

	@Column(name = "productGroup2")
	private String productGroup2;

	@Column(name = "productGroup3")
	private String productGroup3;

	@Column(name = "visitDate")
	private Date visitDate;
}
