package id.nexcloud.nexsurvey.entity.jpa;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import id.nexcloud.nexsurvey.entity.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "customer")

public class customer extends BaseEntity{

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id", referencedColumnName="id", insertable=true, updatable=true)
	@NotFound(action=NotFoundAction.IGNORE)
	private List<implementationPromo> listPromo;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id", referencedColumnName="id", insertable=true, updatable=true)
	@Fetch(value = FetchMode.SUBSELECT)
	@NotFound(action=NotFoundAction.IGNORE)
	private List<mustSellList> listMsl;

	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id", referencedColumnName="id", insertable=true, updatable=true)
	@Fetch(value = FetchMode.SUBSELECT)
	@NotFound(action=NotFoundAction.IGNORE)
	private List<noteStore> listNote;
	
	@OneToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id", referencedColumnName="id", insertable=true, updatable=true)
	@Fetch(value = FetchMode.SUBSELECT)
	@NotFound(action=NotFoundAction.IGNORE)
	private List<productInStore> listProd;
	
	@Column(name = "principalID")
	private String principalID;
	
	@Column(name = "distributorID")
	private String distributorID;
	
	@Column(name = "customerCode")
	private String customerCode;
	
	@Column(name = "customerName")
	private String customerName;
	
	@Column(name = "customerType")
	private String customerType;
	
	@Column(name = "customerSubtype")
	private String customerSubtype;
	
	@Column(name = "visitDate")
	private Date visitDate;
}
