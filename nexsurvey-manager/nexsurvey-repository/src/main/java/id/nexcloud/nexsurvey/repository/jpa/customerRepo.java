package id.nexcloud.nexsurvey.repository.jpa;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import id.nexcloud.nexsurvey.entity.jpa.customer;

public interface customerRepo  extends JpaRepository<customer, Long>, QuerydslPredicateExecutor<customer>{
	
	List<customer> findByPrincipalIDAndDistributorID(String principalID, String distributorID);
	
	List<customer> findByPrincipalIDAndDistributorIDAndCustomerCode(String principalID, String distributorID, String customerID);
	customer findByPrincipalIDAndDistributorIDAndCustomerCodeAndVisitDate(String principalID, String distributorID, String customerID,Date visitDate);

}
