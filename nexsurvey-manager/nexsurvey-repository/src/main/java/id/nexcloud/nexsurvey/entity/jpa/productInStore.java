package id.nexcloud.nexsurvey.entity.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import id.nexcloud.nexsurvey.entity.common.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "productInStore")

public class productInStore extends BaseEntity {

	@Column(name = "customer_ID")
	private Long customer_ID;
	
	@Column(name = "productCode")
	private String productCode;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "goodStock")
	private Integer goodStock;
	
	@Column(name = "badStock")
	private Integer badStock;
	
	@Column(name = "sellingPrice")
	private Double sellingPrice;

	@Column(name = "visitDate")
	private Date visitDate;
}
