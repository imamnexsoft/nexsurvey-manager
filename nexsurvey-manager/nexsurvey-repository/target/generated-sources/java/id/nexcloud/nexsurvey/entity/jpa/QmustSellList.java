package id.nexcloud.nexsurvey.entity.jpa;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QmustSellList is a Querydsl query type for mustSellList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QmustSellList extends EntityPathBase<mustSellList> {

    private static final long serialVersionUID = -570393123L;

    public static final QmustSellList mustSellList = new QmustSellList("mustSellList");

    public final id.nexcloud.nexsurvey.entity.common.QBaseEntity _super = new id.nexcloud.nexsurvey.entity.common.QBaseEntity(this);

    //inherited
    public final StringPath createdBy = _super.createdBy;

    //inherited
    public final DateTimePath<java.util.Date> createdDate = _super.createdDate;

    public final NumberPath<Long> customer_ID = createNumber("customer_ID", Long.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final StringPath isAvailable = createString("isAvailable");

    //inherited
    public final StringPath modifiedBy = _super.modifiedBy;

    //inherited
    public final DateTimePath<java.util.Date> modifiedDate = _super.modifiedDate;

    public final StringPath productCode = createString("productCode");

    public final StringPath productGroup1 = createString("productGroup1");

    public final StringPath productGroup2 = createString("productGroup2");

    public final StringPath productGroup3 = createString("productGroup3");

    public final StringPath productName = createString("productName");

    public final DateTimePath<java.util.Date> visitDate = createDateTime("visitDate", java.util.Date.class);

    public QmustSellList(String variable) {
        super(mustSellList.class, forVariable(variable));
    }

    public QmustSellList(Path<? extends mustSellList> path) {
        super(path.getType(), path.getMetadata());
    }

    public QmustSellList(PathMetadata metadata) {
        super(mustSellList.class, metadata);
    }

}

